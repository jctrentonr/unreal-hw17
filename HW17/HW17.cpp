﻿
#include <iostream>

class Vector
{
private:
	double _x = 0;
	double _y = 0;
	double _z = 0;
public:

	Vector(double x, double y, double z) : _x(x), _y(y), _z(z)
	{	}

	double x() const
	{
		return _x;
	}

	double y() const
	{
		return _y;
	}

	double z() const
	{
		return _z;
	}

	void ShowCoords()
	{
		std::cout << "x = " << x() << ", y = " << y() << ", z = " << z() << "\n";
	}

	// Длина вектора вычисляется по формуле √( x2 + y2 + z2) 
	double GetLength()
	{
		return sqrt(_x*_x + _y*_y + _z*_z);
	}
};

int main()
{
	Vector v(1,2,3);
	v.ShowCoords();

	std::cout << "Length: " << v.GetLength();
}